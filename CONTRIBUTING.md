# CONTRIBUTING

## Setup
* Install Node.js and Apache Cordova.
* Setup Android SDK.


## Getting Start
```
$ git clone ...
```

Open two consoles.

```
# first console
$ cd myApp
$ cd webapp
webapp$ npm install
webapp$ npm run watch
```

```
# second console
$ cd myApp
$ cd cordova
cordova$ cordova prepare --fetch
cordova$ cordova run browser -- --live-reload
```

Edit 'webapp/app/js/components/hello-cordova.js' files, live reload browser.


## Project structure
| Path                          | Description
| ----------------------------- | ----
| `cordova`                     | Apache Cordova project.
| `cordova/build.json`          | Default build settings.
| `cordova/build.unsigned.json` | Unsigned release build settings.
| `cordova/android.keystore`    | Debug build key store for android.
| `webapp`                      | Web application source codes.
| `webapp/app`                  | App javascript, css and images.


## References
### Build app
Debug build apk. (Shared key)

```
webapp$ npm run build
cordova$ cordova build android
```

Unsigned release build apk.

```
webapp$ npm run build
cordova$ cordova build android --release --buildConfig build.unsigned.json
```

Signed release build apk.

```
webapp$ npm run build
cordova$ cordova build android --release
```

Edit `keystore` and etc in `build.json`.

Manual sign.

```
webapp$ npm run build
cordova$ cordova build android --release --buildConfig build.unsigned.json
cordova$ cp platforms/android/build/outputs/apk/android-release-unsigned.apk platforms/android/build/outputs/apk/android-release.apk
cordova$ jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore /path/to/brand.keystore platforms/android/build/outputs/apk/android-release.apk myBrand
```


### Live reload
Live reload in brower.

```
webapp$ npm run watch
cordova$ cordova emulate browser -- --live-reload
```

Live reload in Android emulator.

```
webapp$ npm run watch
cordova$ cordova emulate android -- --live-reload
```

Live reload in Android emulator (avd1).

```
webapp$ npm run watch
cordova$ cordova emulate android --target=avd1 -- --live-reload
```

Live reload in Android device.

```
webapp$ npm run watch
cordova$ cordova run android -- --live-reload
```


### Test
Run karma test.

```
webapp$ npm run test
```
