import angular from 'angular';

angular.module('myApp')
  .component('helloCordova', {
    template: `
<div>
  <h1>Apache Cordova</h1>
  <div id="deviceready" class="blink">
    <p class="event listening" ng-hide="$ctrl.isReady">Connecting to Device</p>
    <p class="event received" ng-show="$ctrl.isReady">Device is Ready</p>
  </div>
</div>
`,
    controller($scope, $log) {
      this.isReady = false;

      const onDeviceReady = () => {
        setTimeout(() => {
          $scope.$apply(() => this.isReady = true);
        }, 5000);
        $log.log('Received Event: deviceready');
      };

      document.addEventListener('deviceready', onDeviceReady, false);
    },
  });
