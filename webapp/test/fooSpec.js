import Foo from '../app/js/foo.js';

describe('Foo', () => {
  describe('hello', () => {
    it('hello', () => {
      expect(new Foo().hello()).toEqual('Hello!');
    });
  });
});