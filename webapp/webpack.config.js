const path = require('path');

module.exports = {
  entry: path.resolve(__dirname, 'app/index.js'),
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, '../cordova/www')
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['es2015']
        }
      }, {
        test: /\.css$/,
        loader: ['style-loader', 'css-loader'],
      }, {
        test: /\.(png|jpg|gif)$/,
        loader: 'url-loader',
      },
    ],
  },
};
